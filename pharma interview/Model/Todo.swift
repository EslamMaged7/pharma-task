//
//  Todo.swift
//  pharma interview
//
//  Created by apple on 9/22/20.
//  Copyright © 2020 Eslam Maged. All rights reserved.
//

import Foundation
import Firebase

struct Todo : Codable {
    
    let created_at : Timestamp?
    let title : String?
    let details : String?
    let date : String?
    let is_reminder : Bool?
    
}
