//
//  TodoTableViewCell.swift
//  pharma interview
//
//  Created by apple on 9/22/20.
//  Copyright © 2020 Eslam Maged. All rights reserved.
//

import UIKit

class TodoTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(item: Todo) {
        titleLabel.text = item.title
        detailsLabel.text = item.details
        dateLabel.text = item.date
        dateLabel.isHidden = item.is_reminder == true ? false : true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
