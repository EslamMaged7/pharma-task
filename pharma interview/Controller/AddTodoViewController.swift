//
//  AddTodoViewController.swift
//  pharma interview
//
//  Created by apple on 9/22/20.
//  Copyright © 2020 Eslam Maged. All rights reserved.
//

import UIKit
import FirebaseFirestore
import UserNotifications

class AddTodoViewController: UIViewController {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var detailsTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    
    let datePickerView = UIDatePicker()
    var isReminder = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    func setupUI() {
        datePickerView.datePickerMode = .dateAndTime
        dateTextField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(datePickerView:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(datePickerView: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
        dateTextField.text = dateFormatter.string(from: datePickerView.date)
    }
    
}

// MARK: Actions
extension AddTodoViewController {
    
    @IBAction func isReminderAction(_ sender: UISwitch) {
        isReminder = sender.isOn
        dateTextField.isHidden = !sender.isOn
        
    }
    
    @IBAction func addTodoAction(_ sender: Any) {
        guard validateInputs() else {return}
        AddTodoToFirestore(title: titleTextField.text!, details: detailsTextField.text!, date: dateTextField.text ?? "", is_reminder: isReminder)
    }
    
}

// MARK: Validation
extension AddTodoViewController {
    
    private func validateInputs() -> Bool {
        view.endEditing(true)
        guard titleTextField?.text != "" else {
            return false
        }
        guard detailsTextField?.text != "" else {
            return false
        }
        guard dateTextField?.text != "" else {
            if isReminder == false {
                return true
            }else {
                return false
            }
        }
        return true
    }
    
}

// MARK: Firestore
extension AddTodoViewController {
    
    func AddTodoToFirestore(title: String, details: String, date: String, is_reminder: Bool) {
        Firestore.firestore().collection("Pharma").document("Pharma").updateData([
            "data": FieldValue.arrayUnion([[
                "created_at" : Timestamp(date: Date()),
                "title" : title,
                "details" : details,
                "date" : date,
                "is_reminder" : is_reminder,
                ]])
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
                Firestore.firestore().collection("Pharma").document("Pharma").setData([
                    "data": [[
                        "created_at" : Timestamp(date: Date()),
                        "title" : title,
                        "details" : details,
                        "date" : date,
                        "is_reminder" : is_reminder,
                        ]],
                ]) { err in
                    if let err = err {
                        print("Error writing document: \(err)")
                    } else {
                        print("Document successfully written!")
                        self.titleTextField.text = ""
                        self.detailsTextField.text = ""
                        self.dateTextField.text = ""
                        self.messageLabel.text = "Todo successfully added"
                        self.messageLabel.isHidden = false
                        print(date)
                        self.setReminder(title: title, body: details, date: date)
                    }
                }
            } else {
                print("Document successfully updated")
                self.titleTextField.text = ""
                self.detailsTextField.text = ""
                self.dateTextField.text = ""
                self.messageLabel.text = "Todo successfully added"
                self.messageLabel.isHidden = false
                print(date)
                self.setReminder(title: title, body: details, date: date)
            }
        }
    }
    
}

// MARK: UserNotifications
extension AddTodoViewController {
    
    func setReminder(title: String, body: String, date: String) {
        if isReminder {
            
            let center = UNUserNotificationCenter.current()
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.sound = .default
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm dd/MM/yyyy"
            let date = dateFormatter.date(from: date)!
            
            let fireDate = Calendar.current.dateComponents([.day, .month, .year, .hour, .minute], from: date)
            let trigger = UNCalendarNotificationTrigger(dateMatching: fireDate, repeats: false)
            let request = UNNotificationRequest(identifier: "reminder", content: content, trigger: trigger)
            center.add(request) { (error) in
                if error != nil {
                    print("Error = \(error?.localizedDescription ?? "error local notification")")
                }
            }
            
        }
    }
    
}
