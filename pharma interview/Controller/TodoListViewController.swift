//
//  TodoListViewController.swift
//  pharma interview
//
//  Created by apple on 9/22/20.
//  Copyright © 2020 Eslam Maged. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseFirestoreSwift

class TodoListViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
        
    var todoArray = [Todo]()
    var todoSearchArray = [Todo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startFirestoreListener()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopFirestoreListener()
    }
        
    func setupUI() {
        
        searchBar.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TodoTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.tableFooterView = UIView()
                
    }
    
    func updateUI(data: [Todo]) {
        todoArray = data
        todoSearchArray = todoArray
        tableView.reloadData()
    }

}

// MARK: Actions
extension TodoListViewController {
    
    @IBAction func addTodoAction(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddTodoViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK: SearchBar
extension TodoListViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            todoArray = todoSearchArray
            tableView.reloadData()
            return
        }
        todoArray = todoSearchArray.filter({ item -> Bool in
            (item.title?.lowercased().contains(searchText.lowercased()) ?? true || item.details?.lowercased().contains(searchText.lowercased()) ?? true)
        })
        tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        self.view.endEditing(true)
    }
    
}

// MARK: TableView
extension TodoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TodoTableViewCell
        cell.configure(item: todoArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteTodoFromFireStore(element: todoArray[indexPath.row])
            todoArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
}

// MARK: Firestore
extension TodoListViewController {
    
    func startFirestoreListener() {
        Firestore.firestore().collection("Pharma").document("Pharma")
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("Document data was empty.")
                    return
                }
                print("Current data: \(data)")
                let result = Result {
                    try document.data(as: TodoModel.self)
                }
                switch result {
                case .success(let response):
                    if let data = response {
                        self.updateUI(data: data.data ?? [])
                    } else {
                        print("Document does not exist")
                    }
                case .failure(let error):
                    print("Error decoding TodoModel: \(error)")
                }
        }
    }
    
    func stopFirestoreListener() {
        let listener = Firestore.firestore().collection("Pharma").addSnapshotListener { querySnapshot, error in
        }
        listener.remove()
    }
    
    func deleteTodoFromFireStore(element: Todo) {
        Firestore.firestore().collection("Pharma").document("Pharma").updateData([
            "data": FieldValue.arrayRemove([["title": element.title!, "details": element.details!, "created_at": element.created_at!, "date": element.date!, "is_reminder": element.is_reminder!]])
        ]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
            }
        }
    }
    
}
